<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.8.17">
  <compounddef id="NoTypeTensor_8cuh" kind="file" language="C++">
    <compoundname>NoTypeTensor.cuh</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">/**</highlight></codeline>
<codeline><highlight class="normal"><sp/>*<sp/>Copyright<sp/>(c)<sp/>Facebook,<sp/>Inc.<sp/>and<sp/>its<sp/>affiliates.</highlight></codeline>
<codeline><highlight class="normal"><sp/>*</highlight></codeline>
<codeline><highlight class="normal"><sp/>*<sp/>This<sp/>source<sp/>code<sp/>is<sp/>licensed<sp/>under<sp/>the<sp/>MIT<sp/>license<sp/>found<sp/>in<sp/>the</highlight></codeline>
<codeline><highlight class="normal"><sp/>*<sp/>LICENSE<sp/>file<sp/>in<sp/>the<sp/>root<sp/>directory<sp/>of<sp/>this<sp/>source<sp/>tree.</highlight></codeline>
<codeline><highlight class="normal"><sp/>*/</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">#pragma<sp/>once</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">#include<sp/>&lt;faiss/impl/FaissAssert.h&gt;</highlight></codeline>
<codeline><highlight class="normal">#include<sp/>&lt;faiss/gpu/utils/Tensor.cuh&gt;</highlight></codeline>
<codeline><highlight class="normal">#include<sp/>&lt;initializer_list&gt;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">namespace<sp/>faiss<sp/>{</highlight></codeline>
<codeline><highlight class="normal">namespace<sp/>gpu<sp/>{</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">template<sp/>&lt;int<sp/>Dim,<sp/>bool<sp/>InnerContig<sp/>=<sp/>false,<sp/>typename<sp/>IndexT<sp/>=<sp/>int&gt;</highlight></codeline>
<codeline><highlight class="normal">class<sp/>NoTypeTensor<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/>public:</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor()<sp/>:<sp/>mem_(nullptr),<sp/>typeSize_(0)<sp/>{}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>template<sp/>&lt;typename<sp/>T&gt;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor(Tensor&lt;T,<sp/>Dim,<sp/>InnerContig,<sp/>IndexT&gt;&amp;<sp/>t)</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>:<sp/>mem_(t.data()),<sp/>typeSize_(sizeof(T))<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>i<sp/>=<sp/>0;<sp/>i<sp/>&lt;<sp/>Dim;<sp/>++i)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>size_[i]<sp/>=<sp/>t.getSize(i);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[i]<sp/>=<sp/>t.getStride(i);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor(void*<sp/>mem,<sp/>int<sp/>typeSize,<sp/>std::initializer_list&lt;IndexT&gt;<sp/>sizes)</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>:<sp/>mem_(mem),<sp/>typeSize_(typeSize)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>int<sp/>i<sp/>=<sp/>0;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(auto<sp/>s<sp/>:<sp/>sizes)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>size_[i++]<sp/>=<sp/>s;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[Dim<sp/>-<sp/>1]<sp/>=<sp/>(IndexT)1;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>j<sp/>=<sp/>Dim<sp/>-<sp/>2;<sp/>j<sp/>&gt;=<sp/>0;<sp/>--j)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[j]<sp/>=<sp/>stride_[j<sp/>+<sp/>1]<sp/>*<sp/>size_[j<sp/>+<sp/>1];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor(void*<sp/>mem,<sp/>int<sp/>typeSize,<sp/>int<sp/>sizes[Dim])</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>:<sp/>mem_(mem),<sp/>typeSize_(typeSize)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>i<sp/>=<sp/>0;<sp/>i<sp/>&lt;<sp/>Dim;<sp/>++i)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>size_[i]<sp/>=<sp/>sizes[i];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[Dim<sp/>-<sp/>1]<sp/>=<sp/>(IndexT)1;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>i<sp/>=<sp/>Dim<sp/>-<sp/>2;<sp/>i<sp/>&gt;=<sp/>0;<sp/>--i)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[i]<sp/>=<sp/>stride_[i<sp/>+<sp/>1]<sp/>*<sp/>sizes[i<sp/>+<sp/>1];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor(</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>void*<sp/>mem,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>int<sp/>typeSize,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>IndexT<sp/>sizes[Dim],</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>IndexT<sp/>strides[Dim])</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>:<sp/>mem_(mem),<sp/>typeSize_(typeSize)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>i<sp/>=<sp/>0;<sp/>i<sp/>&lt;<sp/>Dim;<sp/>++i)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>size_[i]<sp/>=<sp/>sizes[i];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>stride_[i]<sp/>=<sp/>strides[i];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>int<sp/>getTypeSize()<sp/>const<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>typeSize_;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>IndexT<sp/>getSize(int<sp/>dim)<sp/>const<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>FAISS_ASSERT(dim<sp/>&lt;<sp/>Dim);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>size_[dim];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>IndexT<sp/>getStride(int<sp/>dim)<sp/>const<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>FAISS_ASSERT(dim<sp/>&lt;<sp/>Dim);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>stride_[dim];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>template<sp/>&lt;typename<sp/>T&gt;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>Tensor&lt;T,<sp/>Dim,<sp/>InnerContig,<sp/>IndexT&gt;<sp/>toTensor()<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>FAISS_ASSERT(sizeof(T)<sp/>==<sp/>typeSize_);</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>Tensor&lt;T,<sp/>Dim,<sp/>InnerContig,<sp/>IndexT&gt;((T*)mem_,<sp/>size_,<sp/>stride_);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>NoTypeTensor&lt;Dim,<sp/>InnerContig,<sp/>IndexT&gt;<sp/>narrowOutermost(</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>IndexT<sp/>start,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>IndexT<sp/>size)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>char*<sp/>newPtr<sp/>=<sp/>(char*)mem_;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>if<sp/>(start<sp/>&gt;<sp/>0)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>newPtr<sp/>+=<sp/>typeSize_<sp/>*<sp/>start<sp/>*<sp/>stride_[0];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>IndexT<sp/>newSize[Dim];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>for<sp/>(int<sp/>i<sp/>=<sp/>0;<sp/>i<sp/>&lt;<sp/>Dim;<sp/>++i)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>if<sp/>(i<sp/>==<sp/>0)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>assert(start<sp/>+<sp/>size<sp/>&lt;=<sp/>size_[0]);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>newSize[i]<sp/>=<sp/>size;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}<sp/>else<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>newSize[i]<sp/>=<sp/>size_[i];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>NoTypeTensor&lt;Dim,<sp/>InnerContig,<sp/>IndexT&gt;(</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>newPtr,<sp/>typeSize_,<sp/>newSize,<sp/>stride_);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/>private:</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>void*<sp/>mem_;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>int<sp/>typeSize_;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>IndexT<sp/>size_[Dim];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>IndexT<sp/>stride_[Dim];</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">}<sp/>//<sp/>namespace<sp/>gpu</highlight></codeline>
<codeline><highlight class="normal">}<sp/>//<sp/>namespace<sp/>faiss</highlight></codeline>
    </programlisting>
    <location file="faiss/gpu/utils/NoTypeTensor.cuh"/>
  </compounddef>
</doxygen>
